import React from 'react';
import './CountryCard.css';

function CountryCard(props){

    return (
        <div className='countryCard'>
            <section className='img'></section>
            {/* <img src={props.flags.svg} width='auto' height='auto' alt="" /> */}
            <section style={{padding: '2rem'}}>
                <h4>{props.name.official}</h4>
                <h5>Population: {props.population}</h5>
                <h5>Region: {props.region}</h5>
                <h5>Capital: {props.capital}</h5>
            </section>
        </div>
    );
}
 
export default CountryCard;