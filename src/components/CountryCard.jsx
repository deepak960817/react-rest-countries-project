import React from 'react';
import './CountryCard.css';
import { Link }  from "react-router-dom"

function CountryCard(props){
    if(props.validCountry.length !== 0)
    {
        return (
            <div style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center'}}>
                {props.validCountry.map((country) => {
                    let Name=country.name.common;
                return (
                    <Link to={`/${Name}`} onClick={() => {props.path(`${Name}`)}} className='link' key={country['name']['official']} style={{display: "flex", width:"20%"}} >
                        <div className='countryCard' style={{display: 'flex', width: '100%', flexDirection: 'column', justifyContent: 'space-between'}}> 
                        <img src={country.flags.svg} width='100%' height='50%' alt="" />
                        <section style={{padding: '2rem'}}>
                            <h3>{country.name.official}</h3>
                            <h5>Population: {country.population}</h5>
                            <h5>Region: {country.region}</h5>
                            <h5>Capital: {country.capital}</h5>
                        </section>
                        </div>
                    </Link>
                )
            })}
            </div>
        )
    }
    else
    {
        return(
            <div style={{display: 'flex', justifyContent: 'center'}}>
                <h1>Sorry, No such country in the region</h1>;
            </div>
        )
        
    }
}
 
export default CountryCard;