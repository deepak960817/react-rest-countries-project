import React, {Component} from 'react';

class FilterCountries extends Component {
    render() { 
        return (
            <select onChange={(e) => this.props.textChange(e.target.value)} className='filterBox' type="text" style={{width: '15%'}}>
                <option>Filter by Region</option>
                <option>Americas</option>
                <option>Asia</option>
                <option>Europe</option>
                <option>Oceania</option>
                <option>Africa</option>
                <option>Antarctic</option>
            </select>
     )
    }
}
 
export default FilterCountries;