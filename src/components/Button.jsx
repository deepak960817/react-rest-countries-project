import React from 'react';
import { useNavigate } from 'react-router-dom';

const Button = (props) => {
    let navigate = useNavigate();
    return (  
        <button onClick={() => {props.path(props.value)}} style={props.style}>{props.btn}</button>
    );
}
 
export default Button;