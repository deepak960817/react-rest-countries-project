import React, { Component } from 'react';
import Button from './Button';
import './CountryPage.css';
import {Link} from 'react-router-dom';

class CountryPage extends Component {

    GettingCountryData = () => {
        const countryObj = this.props.countryList.reduce((acc,curr) => {
            if(curr.name.common === this.props.name)
            {
                acc=curr;
            }
            return acc;
        },[])
        return(countryObj)
    }

    NativeName = () =>{
        const nativeNameObject = this.GettingCountryData().name.nativeName;
        const nativeName = Object.values(nativeNameObject)[0].official;
        return nativeName;
    }

    Currencies = () => {
        const currencies = this.GettingCountryData().currencies;
        const currency = currencies.name;
        return currency;
    }

    Languages = () => {
        const languages = this.GettingCountryData().languages;
        const language = Object.values(languages).map((val) => { return val+" "});
        return language;
    }

    BorderCountries = () => {
        if('borders' in this.GettingCountryData())
        {
            const borderCountries = this.GettingCountryData().borders.map((item) => {
                for(let index=0; index<this.props.border.length; index++)
                {
                    if((Object.keys(this.props.border[index])).toString() === item)
                    {
                        return (Object.values(this.props.border[index])).toString();
                    }
                }
            })
            return borderCountries;
        }
        return [];
    }

    render() { 
    //    console.log(this.borderCountries())
            return (
                <React.Fragment >
                    <section className='countryPageSection'>
                    <Link to={'/'}>
                    <Button  btn={"<- Back"} value={'/'} path={this.props.path} style={{marginBottom:'5%', height: '2rem', alignText: 'center', boxShadow: '0px 0px 1px black', backgroundColor: 'white'}}/>
                    </Link>
                    
                    <section className='countryData'>
                        <section className='imgSection' >
                            <img src={this.GettingCountryData().flags.svg} width='80%' alt="" />
                        </section>
                        <section className='contentSection'>
                            <h1 className='countryName' style={{fontSize: '1.5rem'}}>{this.GettingCountryData().name.common}</h1>
                            <div className='countryInfo'>
                            <ul className='column'>
                                <li><b>Native Name:</b> {this.NativeName()};</li>
                                <li><b>Population:</b> {this.GettingCountryData().population}</li>
                                <li><b>Region:</b> {this.GettingCountryData().region}</li>
                                <li><b>SubRegion:</b> {this.GettingCountryData().subregion}</li>
                                <li><b>Capital:</b> {this.GettingCountryData().capital}</li>
                            </ul>
                            <ul className='column'>
                                <li><b>Top Level Domain:</b> {this.GettingCountryData().tld}</li>
                                <li><b>Currencies:</b> {this.Currencies()}</li>
                                <li><b>Languages:</b> {this.Languages()}</li>
                            </ul>
                            </div>
                            <div className='borderCountrySection'>
                                <h4>Border Countries</h4>
                                <div className='borderCountries'>
                                    {/* <Button btn={"ABC"} style={{width: '15%', height: '1.5rem', alignText: 'center', boxShadow: '0px 0px 1px grey', backgroundColor: 'white', }}/>
                                    <Button btn={"ABC"} style={{width: '15%', height: '1.5rem', alignText: 'center', boxShadow: '0px 0px 1px grey', backgroundColor: 'white', }}/>
                                    <Button btn={"ABC"} style={{width: '15%', height: '1.5rem', alignText: 'center', boxShadow: '0px 0px 1px grey', backgroundColor: 'white', }}/> */}
                                    {this.BorderCountries().map((val) => {
                                    {
                                        return(
                                            <Link to={`/${val}`} className='borderCountryLink' style={{display: 'flex', height: '30%'}} >
                                                <Button btn={`${val}`} value={`${val}`} path={this.props.path} style={{height: '2rem', alignText: 'center', boxShadow: '0px 0px 1px grey', backgroundColor: 'white', }}/>
                                            </Link>
                                        )
                                    }
                                })}
                                </div>
                                
                            </div>
                        </section>
                    </section>
                    </section>
                </React.Fragment>
            );
    }
}
 
export default CountryPage;


