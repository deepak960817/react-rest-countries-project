import React from 'react';

const SearchBar = (props) => {
    return ( 
        <input onChange={(e) => props.content(e.target.value)} type="text" placeholder='Search for a country' style={{width: '25%'}}/>
     );
}
 
export default SearchBar;