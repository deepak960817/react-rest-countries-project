import React, { Component } from 'react';
import './App.css';
import axios from 'axios'
import HeaderBar from './components/HeaderBar';
import CountryCard from './components/CountryCard';
import FilterCountries from './components/FilterCountries';
import SearchBar from './components/SearchBar';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import CountryPage from './components/CountryPage';

let borderCountries = [];
class App extends Component {

  state = { 
    CountryCard : [],
    SearchedCountries: "",
    countryList: [],
    webSearchBar: "/",
    pageMount: true,
    pathName: "/"
  } 

  componentDidMount(){
    axios.get('https://restcountries.com/v3.1/all').then(result => {
    this.setState({CountryCard: result.data, pageMount: false, countryList: result.data}, () => {borderCountries = result.data.reduce((acc, curr) => {
      let newEntry = {};
      newEntry[curr['cca3']]= curr['name']['common'];
      acc.push(newEntry);
      return acc;
    },[])})
  })
}

  handleFilter = (val) => {
    const searchtext = val.toLowerCase();
    if(searchtext !== 'filter by region')
    {
      const filteredCountries = this.state.CountryCard.filter((country) => {
      return(country.region.toLowerCase().includes(searchtext))
      })

    this.setState({CountryCard: filteredCountries});
    }
    else
    {
      this.setState({CountryCard: this.state.CountryCard});
    }
  }

  searchByCountry = (input) => {
    this.setState({SearchedCountries: input.toLowerCase()});
  }

  path = (countrypath) => {
    if(countrypath)
    {this.setState({pathName: countrypath})}
    return this.state.pathName;
  }
  
  render() { 
    
    let validCountries = this.state.CountryCard.filter((val) => {
      return (this.state.SearchedCountries === "" || val.name.official.toLowerCase().includes(this.state.SearchedCountries));
    })
    let finalPath="";
    let countrytoShow;
    let manualPath= window.location.pathname.split('/');
    if(manualPath[manualPath.length -1] === ""){finalPath = '/'}
    else
    { 
      finalPath = manualPath[manualPath.length -1];
      if(manualPath[manualPath.length -1].includes('%')){countrytoShow = manualPath[manualPath.length -1].replaceAll("%20", " ")}
      else{countrytoShow = manualPath[manualPath.length -1]}
    }
    
      return ( this.state.pageMount ? <div style={{display: 'flex', justifyContent: 'center'}}><img src="https://www.kindpng.com/picc/m/19-197708_loading-please-wait-png-transparent-png.png" alt=""/></div> :

      <React.Fragment>
        <div className='header'>
          <HeaderBar />
        </div>
        <Router>
          <Routes>
          {finalPath === '/' ? 
          <Route path={`/${finalPath}`} element={
            <section>
              <div style={{display: 'flex', justifyContent: 'space-between', height:'3rem', marginTop: '2%', marginLeft: '5%', marginRight: '5%'}}>
              <SearchBar content={this.searchByCountry}/>
              <FilterCountries textChange={this.handleFilter}/>
              </div>
              <div style={{marginLeft: '3%', marginRight: '3%'}}>
                <CountryCard validCountry={Object.values(validCountries)} path={this.path}  />
              </div>
          </section>
          } />
          :
          <Route path={`/${finalPath}`} element={
            <CountryPage name={countrytoShow} countryList={(this.state.countryList)} border={(borderCountries)} path={this.path}/>
          } /> 
        }
          </Routes>
        </Router> 
    </React.Fragment>
      )
    }
}

export default App;
